package com.test.ble;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.test.ble.adapter.AdapterDeviceList;
import com.test.ble.event.DeviceEvent;
import com.test.ble.model.DeviceModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private final static int PERMISSION_REQUEST=1;

    private BluetoothAdapter bleAdapter;
    private BluetoothLeScanner scanner;
    private ArrayList<DeviceModel> devices=new ArrayList();
    private AdapterDeviceList adapterDeviceList;
    private LocationManager manager;
    private boolean isScanning;

    private RecyclerView recyclerScan;
    private Button btnScan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        EventBus.getDefault().register(this);

        recyclerScan=(RecyclerView)findViewById(R.id.recycler_view);
        btnScan=(Button) findViewById(R.id.btn_scan);

        recyclerScan.setLayoutManager(new LinearLayoutManager(this));
        adapterDeviceList=new AdapterDeviceList(devices);
        recyclerScan.setAdapter(adapterDeviceList);

        initLocationManager();
        checkBleAdapter();
        requestPerms();
        checkCurrentScanState(ScanService.isServiceCreated());
    }

    private void checkBleAdapter(){
        try {
            final BluetoothManager bluetoothManager =
                    (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            bleAdapter = bluetoothManager.getAdapter();
        }catch (Exception e){}
    }

    private void initLocationManager(){
        manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE );
    }

    private void checkCurrentScanState(boolean isServiceRunning){
        if(isServiceRunning){
            isScanning=true;
            btnScan.setText(getResources().getString(R.string.main_btn_stop_scan));
            devices.addAll(ScanService.hashMap.values());
            adapterDeviceList.notifyDataSetChanged();
        }
    }

    private void requestPerms(){
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE},
                PERMISSION_REQUEST);
    }

    public void setScanModeClick(View v){
        Button button=(Button)v;
        Intent intent = new Intent(this, ScanService.class);
        if(isScanning){
            this.stopService(intent);
            isScanning=false;
            button.setText(getResources().getString(R.string.main_btn_start_scan));
        }else{
            if(!checkReady()){ // Check permissions, ble and gps status
                return;
            }
            this.startService(intent);
            isScanning=true;
            button.setText(getResources().getString(R.string.main_btn_stop_scan));
        }
    }

    private boolean checkReady(){
        // Check coarse location perm
        if(ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED){
            createToast(getResources().getString(R.string.main_alert_location));
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    1);
            return false;
        }

        // Check external storage perm
        if(ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED){
            createToast(getResources().getString(R.string.main_alert_external));
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    1);
            return false;
        }

        if(ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED){
            createToast(getResources().getString(R.string.main_alert_external));
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    1);
            return false;
        }

        // Check ble enable
        if (bleAdapter == null || !bleAdapter.isEnabled()) {
            createToast(getResources().getString(R.string.main_alert_ble));
            return false;
        }
        if(bleAdapter.getState()!=BluetoothAdapter.STATE_ON){
            createToast(getResources().getString(R.string.main_alert_ble));
            return false;
        }

        if(!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            createToast(getResources().getString(R.string.main_alert_gps));
            return false;
        }
        return true;
    }

    // Get information from ble scan
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(DeviceEvent event) {
        devices.clear();
        devices.addAll(event.hashMap.values());
        adapterDeviceList.notifyDataSetChanged();
    };

    private void createToast(String message){
        Toast.makeText(this, message,
                Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
