package com.test.ble.event;

import com.test.ble.model.DeviceModel;

import java.util.HashMap;

public class DeviceEvent {
    public final HashMap<String, DeviceModel> hashMap;

    public DeviceEvent(HashMap<String, DeviceModel> hashMap){
        this.hashMap=hashMap;
    }
}
