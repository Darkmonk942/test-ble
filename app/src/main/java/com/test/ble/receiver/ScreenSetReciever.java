package com.test.ble.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.util.Log;

import com.test.ble.ScanService;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class ScreenSetReciever extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
            writeToFile();
        } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
            writeToFile();
        } else if(intent.getAction().equals(Intent.ACTION_ANSWER)) {

        }
    }

    private void writeToFile(){
        File file=new File(Environment.getExternalStorageDirectory(), ScanService.NAME_SCAN_FILE);
        FileWriter fw=null;
        if(!file.exists()){
            return;
        }
        try {
            String separator = System.getProperty("line.separator");
            fw = new FileWriter(file, true);
            fw.write(separator);
            fw.write(separator);
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                if(fw!=null){
                    fw.flush();
                    fw.close();
                }
            }catch (Exception e){}
        }
    }
}
