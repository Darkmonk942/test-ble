package com.test.ble;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Environment;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.test.ble.event.DeviceEvent;
import com.test.ble.model.DeviceModel;
import com.test.ble.receiver.ScreenSetReciever;

import org.greenrobot.eventbus.EventBus;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class ScanService extends Service {
    private final byte[] headerBytes={(byte)'M',(byte)'2',(byte)'C'};
    private final byte[] deviceTypeClassic={(byte)'B',(byte)'C'};
    private final byte[] deviceTypeLowEnergy={(byte)'L',(byte)'E'};
    private final byte[] deviceTypeLowDual={(byte)'H',(byte)'S'};

    public static final String NAME_SCAN_FILE="testblelogfile.txt";
    public static HashMap<String,DeviceModel> hashMap=new HashMap<>();

    private BluetoothAdapter bleAdapter;
    private BluetoothLeScanner scanner;
    private String saveDevices;

    private ScreenSetReciever screenReceiver;
    private ScanCallback scanCallback;
    private ScanFilter.Builder builderFilter;

    private static ScanService mInstance = null;
    public static boolean isServiceCreated() {
        try {
            return mInstance != null && mInstance.ping();
        } catch (NullPointerException e) {
            return false;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        bleAdapter = bluetoothManager.getAdapter();
        scanner=bleAdapter.getBluetoothLeScanner();

        initBleCallback();

        // Get current file
        saveDevices=readFromFile();

        // Get Paired devices
        Set<BluetoothDevice> pairedDevices = bleAdapter.getBondedDevices();
        for (BluetoothDevice device:pairedDevices){
            addDevice(device,true);
        }

        sendData();
        initBleCallback();
        startScan();
        startScreenReceiver();
    }

    private void initBleCallback(){
        scanCallback= new ScanCallback() {
            @Override
            public void onScanResult(int callbackType, ScanResult result) {
                super.onScanResult(callbackType, result);
                Log.d("скан",result.getDevice().getAddress());
                if(!hashMap.keySet().contains(result.getDevice().getAddress())){
                    addDevice(result.getDevice(),false);
                }
            }
        };
    }

    private void startScan(){
        builderFilter = new ScanFilter.Builder();
        List<ScanFilter> filters = Collections.singletonList(builderFilter.build());
        ScanSettings.Builder settingBuilder = new ScanSettings.Builder();
        settingBuilder.setScanMode(ScanSettings.SCAN_MODE_LOW_POWER);
        scanner.startScan(filters,settingBuilder.build(),scanCallback);
    }

    private void startScreenReceiver(){
        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        filter.addAction(Intent.ACTION_ANSWER);
        screenReceiver = new ScreenSetReciever();
        registerReceiver(screenReceiver, filter);
    }

    private void addDevice(BluetoothDevice device,boolean isPaired){
        DeviceModel deviceModel=new DeviceModel();
        deviceModel.name=device.getName();
        deviceModel.address=device.getAddress();
        deviceModel.paired=isPaired;
        deviceModel.deviceType=device.getType();
        hashMap.put(device.getAddress(),deviceModel);
        writeToFile(deviceModel);
        sendData();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void sendData(){
        EventBus.getDefault().postSticky(new DeviceEvent(hashMap));
    }

    private void writeToFile(DeviceModel deviceModel){
        if(saveDevices.contains(getDeviceBytes(deviceModel))){
            return;
        }
        File file=new File(Environment.getExternalStorageDirectory(),NAME_SCAN_FILE);
        FileWriter fw=null;
            try {
                if(!file.exists()){
                    file.createNewFile();
                }
                fw = new FileWriter(file, true);
                fw.write(getDeviceBytes(deviceModel)+" ");
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                try {
                    if(fw!=null){
                        fw.flush();
                        fw.close();
                    }
                }catch (Exception e){}
            }
    }

    private String readFromFile() {
        File file=new File(Environment.getExternalStorageDirectory(),NAME_SCAN_FILE);
        StringBuilder text = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            br.close();
        }
        catch (IOException e) {
        }
        return text.toString();
    }

    public String getDeviceBytes(DeviceModel deviceModel) {
        byte[] macBytes = new byte[6];
        byte[] typeBytes = getDeviceType(deviceModel.deviceType);
        byte[] checkSumBytes = new byte[1];
        String[] macAddressParts = deviceModel.address.split(":");


        for(int i=0; i<6; i++){
            Integer hex = Integer.parseInt(macAddressParts[i], 16);
            macBytes[i] = hex.byteValue();
        }
        byte[] copyHeaderMac = new byte[headerBytes.length + macBytes.length];
        System.arraycopy(headerBytes, 0, copyHeaderMac, 0, headerBytes.length);
        System.arraycopy(macBytes, 0, copyHeaderMac, headerBytes.length, macBytes.length);

        byte[] copyHeaderMacType=new byte[copyHeaderMac.length + typeBytes.length];
        System.arraycopy(copyHeaderMac, 0, copyHeaderMacType, 0, copyHeaderMac.length);
        System.arraycopy(typeBytes, 0, copyHeaderMacType, copyHeaderMac.length, typeBytes.length);

        byte sum = 0;
        for (byte b : copyHeaderMacType) {
            sum ^= b;
        }
        checkSumBytes[0]=sum;

        byte[] finalBytes=new byte[copyHeaderMacType.length+checkSumBytes.length];
        System.arraycopy(copyHeaderMacType, 0, finalBytes, 0, copyHeaderMacType.length);
        System.arraycopy(checkSumBytes, 0, finalBytes, copyHeaderMacType.length, checkSumBytes.length);

        return Arrays.toString(finalBytes);
    }

    private byte[] getDeviceType(int type){
        switch (type){
            case BluetoothDevice.DEVICE_TYPE_CLASSIC:
                return deviceTypeClassic;
            case BluetoothDevice.DEVICE_TYPE_DUAL:
                return deviceTypeLowDual;
            case BluetoothDevice.DEVICE_TYPE_LE:
                return deviceTypeLowEnergy;
        }
        return deviceTypeClassic;
    }

    private boolean ping() {
        return true;
    }

    @Override
    public void onDestroy() {
        mInstance = null;
        hashMap.clear();
        if(screenReceiver!=null){
            try {
                unregisterReceiver(screenReceiver);
            }catch (Exception e){}
        }
        if(scanner!=null){
            scanner.stopScan(scanCallback);
        }
        super.onDestroy();
    }
}