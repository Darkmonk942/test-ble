package com.test.ble.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.test.ble.R;
import com.test.ble.model.DeviceModel;

import java.util.ArrayList;

public class AdapterDeviceList extends RecyclerView.Adapter<AdapterDeviceList.ViewHolder> {

    private ArrayList<DeviceModel> devices;

    public AdapterDeviceList(ArrayList<DeviceModel> devices){
        this.devices=devices;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView textAddress;
        TextView textName;
        TextView paired;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            textAddress=(TextView)itemLayoutView.findViewById(R.id.text_address);
            textName=(TextView)itemLayoutView.findViewById(R.id.text_name);
            paired=(TextView)itemLayoutView.findViewById(R.id.text_paired);
        }
    }

    @NonNull
    @Override
    public AdapterDeviceList.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLayout= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_device,null);
        return new AdapterDeviceList.ViewHolder(itemLayout);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterDeviceList.ViewHolder holder, int position) {
        DeviceModel deviceModel=devices.get(position);
        holder.textAddress.setText(deviceModel.address);

        if(deviceModel.name!=null){
            holder.textName.setText(deviceModel.name);
        }else{
            holder.textName.setText("no name");
        }


        if(deviceModel.paired){
            holder.paired.setVisibility(View.VISIBLE);
        }else{
            holder.paired.setVisibility(View.GONE);
        }
    }


    @Override
    public int getItemCount() {
        return devices.size();
    }
}