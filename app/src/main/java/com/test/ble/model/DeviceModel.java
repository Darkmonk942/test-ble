package com.test.ble.model;

public class DeviceModel {

    public String address;
    public String name;
    public boolean paired;
    public int deviceType;
}
